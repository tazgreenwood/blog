from django.urls import path
from . import views

urlpatterns = [
    path('', views.PostList, name='home'),
    path('category/<int:cat_id>', views.PostListCategory, name='home_category'),
    path('detail/<slug:slug>/', views.PostDetail, name='post_detail'),
]