from django.shortcuts import render
from .models import Post, Category

def PostList(request):
    categories = Category.objects.order_by('title')
    if request.method == 'POST':
        posts = Post.objects.filter(title__icontains=request.POST['param']).filter(status=1).order_by('-created_on')
        context = {
            'posts': posts,
            'categories': categories,
        }
        return render(request, 'index.html', context)

    posts = Post.objects.filter(status=1).order_by('-created_on')
    context = {
        'posts': posts,
        'categories': categories,
    }
    return render(request, 'index.html', context)

def PostListCategory(request, cat_id):
    posts = Post.objects.filter(status=1, category=cat_id).order_by('-created_on')
    categories = Category.objects.order_by('title')
    context = {
        'posts': posts,
        'categories': categories,
    }
    return render(request, 'index.html', context)

def PostDetail(request, slug):
    post = Post.objects.get(slug=slug)
    context = {
        'post': post,
    }
    return render(request, 'post_detail.html', context)